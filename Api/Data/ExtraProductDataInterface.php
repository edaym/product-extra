<?php
declare(strict_types=1);

namespace Eday\ProductExtra\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ExtraProductDataInterface
 *
 * @package Eday\ProductExtra\Api\Data
 */
interface ExtraProductDataInterface extends ExtensibleDataInterface
{
    /**
     * @param int $productId
     * @return void
     */
    public function setProductId(int $productId): void;

    /**
     * Product qty from order array with specific status.
     *
     * @return int
     */
    public function getQty(): int;

    /**
     * The last order date.
     *
     * @return string
     */
    public function getLastOrder(): string;
}
