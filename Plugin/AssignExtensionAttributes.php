<?php
declare(strict_types=1);

namespace Eday\ProductExtra\Plugin;

use Eday\ProductExtra\Api\Data\ExtraProductDataInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductExtensionFactory;

/**
 * Class AssignExtensionAttributes
 * @package Eday\ProductExtra\Plugin
 */
class AssignExtensionAttributes
{
    /**
     * @var ExtraProductDataInterfaceFactory
     */
    private $extraProductData;

    /**
     * @var ProductExtensionFactory
     */
    private $productExtFactory;

    /**
     * AssignExtensionAttributes constructor.
     *
     * @param ExtraProductDataInterfaceFactory $extraProductData
     * @param ProductExtensionFactory $productExtFactory
     */
    public function __construct(
        ExtraProductDataInterfaceFactory $extraProductData,
        ProductExtensionFactory $productExtFactory
    ) {
        $this->extraProductData = $extraProductData;
        $this->productExtFactory = $productExtFactory;
    }

    /**
     * @param ProductRepositoryInterface $subject
     * @param ProductInterface $result
     * @return ProductInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetById(
        ProductRepositoryInterface $subject,
        ProductInterface $result
    ) {
        $extraProductData = $this->extraProductData->create();
        $extraProductData->setProductId($result->getId());
        $extensionAttributes = $result->getExtensionAttributes();

        if (!$extensionAttributes) {
            $extensionAttributes = $this->productExtFactory->create();
        }

        $extensionAttributes->setSalesinfo($extraProductData);

        return $result->setExtensionAttributes($extensionAttributes);
    }
}
