<?php
declare(strict_types=1);

namespace Eday\ProductExtra\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Eday\ProductExtra\Api\Data\ExtraProductDataInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class ExtraProductData
 *
 * @package Eday\ProductExtra\Model
 */
class ExtraProductData extends AbstractModel implements ExtraProductDataInterface
{
    private const ORDER_ITEM_TABLE = 'sales_order_item';
    private const ORDER_TABLE = 'sales_order';

    /** @var ResourceConnection */
    private $resourceConnection;

    /** @var int */
    private $productId;

    /** @var int */
    private $qty;

    /** @var string */
    private $lastOrderDate;

    /** @var string */
    public $status;

    /** @var TimezoneInterface */
    public $timezone;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ResourceConnection $resourceConnection
     * @param TimezoneInterface $timezone
     * @param string $status
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ResourceConnection $resourceConnection,
        TimezoneInterface $timezone,
        string $status,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->timezone = $timezone;
        $this->status = $status;
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @inheritDoc
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @inheritDoc
     */
    public function getQty(): int
    {
        if ($this->qty === null) {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    ['item' => self::ORDER_ITEM_TABLE],
                    ''
                )
                ->columns(['qty_ordered' => new \Zend_Db_Expr('SUM(qty_ordered)')])
                ->join(
                    ['order' => self::ORDER_TABLE],
                    'order.entity_id=item.order_id',
                    ''
                )
            ->where('item.product_id=?', $this->productId);

            if ($this->status) {
                $select->where('order.status=?', $this->status);
            }

            $this->qty = (int) $connection->fetchOne($select);
        }

        return $this->qty;
    }

    /**
     * @inheritDoc
     */
    public function getLastOrder(): string
    {
        if ($this->lastOrderDate === null) {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from(
                    ['item' => self::ORDER_ITEM_TABLE],
                    'created_at'
                )
                ->where('item.product_id=?', $this->productId)
                ->order('order_id DESC')
                ->limit(1);

            $fetchedDate = $connection->fetchOne($select);

            $this->lastOrderDate = (string) $this->timezone->date($fetchedDate)->format(DateTime::DATE_PHP_FORMAT);
        }

        return $this->lastOrderDate;
    }
}
