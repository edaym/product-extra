## ProductExtra module

Module for additional product sales information.

## Installation

Compatibility with Magento 2.3.4

Open terminal and run commands below
**composer install part will work after adding to repo**
```bash
composer require edaym/product-extra

php bin/magento setup:upgrade

php bin/magento setup:di:compile
```

Clean cache manually or with command: 

```bash
php bin/magento cache:flush
```

## Usage

**Module enable**

Run `php bin/magento module:enable Eday_ProductExtra`

**Module description**

After installing module will create an additional extension attribute for product named `salesinfo`.
Which consists next sales information about product:

`getQty(): int` - the total number of this product from all orders with some status

`getLastOrder(): string` - date of last sale